#version 440 core
in vec3 local;

uniform mat4 projection, view, model, scale, size;

void main()
{
	vec4 vertex = vec4(local, 1.f);

	// The order of this matrices multiplication is important
	// projection * view * model
	gl_Position = projection * view * scale * model * size * vertex;
}
