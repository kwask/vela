#version 440 core
in vec3 local;
in vec3 color;
in vec3 normal;

out vec3 in_color;
out vec3 in_normal;
out vec3 frag_pos;

uniform mat4 projection, view, model, scale, size;

void main()
{
	in_color = color;
	in_normal = normal;
	frag_pos = vec3(scale*model*size*vec4(local, 1.f));

	vec4 vertex = vec4(local, 1.f);

	// The order of this matrices multiplication is important
	// projection * view * model
	gl_Position = projection * view * scale * model * size * vertex;
}
