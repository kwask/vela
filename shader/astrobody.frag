#version 440 core
in vec3 in_color;
in vec3 frag_pos;
in vec3 in_normal;

out vec4 color;

uniform vec3 light_pos, light_color, light_luminosity;

void main()
{
	// Ambient Lighting
	float ambient_strength = 0.01f;
	vec3 ambient = ambient_strength*light_color;

	// Diffuse Lighting
	vec3 norm = normalize(in_normal);
	vec3 light_dir = normalize(light_pos-frag_pos);

	float diff = max(dot(norm, light_dir), 0.f);
	vec3 diffuse = diff*light_color;
	vec3 luminosity = diffuse+ambient+light_luminosity;

	color = vec4(luminosity*in_color, 1.0f);
}
