CXX=g++
CFLAGS= -Wall -std=c++14
LDLIBS= -lboost_system -lboost_filesystem -lGLEW -lglfw3 -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -lm -pthread -ldl -lCEGUIBase-0 -lCEGUIOpenGLRenderer-0

BIN=vela.out
SRC=$(wildcard ./src/*.cc)
OBJS=$(SRC:.cc=.o)

all: $(OBJS)
	@echo "Object compilation complete"
	@echo "--------------------"
	@echo "Generating `uname` build"
	@echo "Compiler: `$(CXX) --version`"
	@echo "--------------------"
	@$(CXX) $(CFLAGS) $(OBJS) -o $(BIN) $(LDLIBS)
	@echo "Binary file generated"

%.o: %.cc
	@$(CXX) -c $(CFLAGS) $< -o $@ $(LDLIBS)
	@echo "$< generated"

clean:
	@rm -rf $(OBJS) $(BIN)
	@echo "Compilation files removed"

force:
	@touch $(SRC)
	@echo "Forcing compilation"
	@echo "--------------------"
	@make all
