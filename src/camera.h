#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

struct Camera
{
public:
	// default settings
	static constexpr double YAW = 0.f;
	static constexpr double PITCH = 0.f;
	static constexpr double DISTANCE = 1.496E+8f;
	static constexpr double SPEED = 1.0; // Multiplied by distance to get diff
	static constexpr double MOUSE_SENSITIVITY = 0.1f;
	static constexpr double ZOOM_SENSITIVITY = 0.1f; // Multiplied by distance to get diff
	static constexpr double PITCH_LIMIT = 89.f;
	static constexpr double DISTANCE_LIMIT = 5E+9f;

	glm::vec3 target_pos; // Target position in 3D space
	glm::vec3 front; // The direction the camera is facing, aka the positive z axis
	glm::vec3 right; //  The positive x axis of the camera space
	glm::vec3 up; // The positive y axis of the camera space
	glm::vec3 world_up; // The absolute world up direction

	// Camera orientation
	double pitch = YAW;
	double yaw = PITCH;
	double distance = DISTANCE; // How far from the target pos

	// Camera settings
	double speed = SPEED;
	double mouse_sensitivity = MOUSE_SENSITIVITY;
	double zoom_sensitivity = ZOOM_SENSITIVITY;

	Camera(	glm::vec3 target_pos = glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
			double yaw = YAW,
			double pitch = PITCH,
			double speed = SPEED,
			double distance = DISTANCE,
			double mouse_sensitivity = MOUSE_SENSITIVITY,
			double zoom_sensitivity = ZOOM_SENSITIVITY);

	glm::mat4 getViewMatrix(const double &scale);

private:
	void updateVectors();
};

#endif
