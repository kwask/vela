#ifndef GLHELPERS_H
#define GLHELPERS_H

#include <string>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "vertex.h"

namespace GLHelpers
{
	void windowResize(GLFWwindow* window, int length, int height);
	void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void mouseCallback(GLFWwindow* window, double x, double y);
	void mousepressCallback(GLFWwindow* window, int key, int action, int mods);
	void scrollCallback(GLFWwindow* window, double x_offset, double y_offset);
	void errorCallback( int error, const char* description );
	void checkError(std::string name = "");
}

#endif
