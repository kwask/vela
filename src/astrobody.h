#ifndef ASTROBODY_H
#define ASTROBODY_H

#include <vector>

#include "basic.h"
#include "physical.h"
#include "shader.h"

// Astrobody is an astronomical body
class Astrobody : public Basic
{
public:
	Astrobody(uint64_t identifier);
	Astrobody(uint64_t identifier, const Physical &phys);
	~Astrobody();

	void parse(instruct_set* instructions) override;
	void genOrbitMesh();

	Physical physical;

private:
	// TODO map of layers: [name(string)] = layer(Layer*)
};

#endif
