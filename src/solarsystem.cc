#include <map>
#include <string>

#include "solarsystem.h"
#include "generator.h"

SolarSystem::SolarSystem(uint64_t identifier)
	: Basic(identifier) {}

void SolarSystem::close()
{
	 // for any solar system cleanup
}

void SolarSystem::parse(instruct_set* instructions)
{

	if(instructions == nullptr)
	{
		return;
	}

	for(unsigned int i = 0;
		i < Generator::MAX_INSTRUCTIONS && !instructions->empty();
		i++)
	{
		std::string command = instructions->front();
		instructions->pop();

		// TODO if/else tree for parsing commands
	}
}
