#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

#include "camera.h"

Camera::Camera(	glm::vec3 target_pos,
		glm::vec3 up,
		double yaw,
		double pitch,
		double speed,
		double distance,
		double mouse_sensitivity,
		double zoom_sensitivity)
	: 	target_pos(target_pos),
		front(glm::vec3(0.0f, 0.0f, -0.0f)),
		up(up),
		world_up(glm::vec3(0.0f, 0.0f, 1.0f)),
		pitch(pitch),
		yaw(yaw),
		distance(distance),
		speed(speed),
		mouse_sensitivity(mouse_sensitivity),
		zoom_sensitivity(zoom_sensitivity)
{
	updateVectors();
}

glm::mat4 Camera::getViewMatrix(const double &scale)
{
	updateVectors();
	glm::vec3 offset = front*(distance*scale/2.f);

	glm::mat4 trans;
	trans = glm::translate(trans, offset);
	glm::mat4 view = glm::lookAt(target_pos-offset, target_pos, up);
	view = view*trans;

	return view;
}

void Camera::updateVectors()
{
	// Calculate new front
	front = glm::normalize(glm::vec3(
		cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
		sin(glm::radians(yaw)) * cos(glm::radians(pitch)),
		sin(glm::radians(pitch))));

	 // Calculate our right and up vector from new front
	right = glm::normalize(glm::cross(front, world_up));
	up = glm::normalize(glm::cross(right, front));
}
