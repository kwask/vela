#ifndef KEYPRESS_H
#define KEYPRESS_H

struct Keypress
{
	Keypress(int key, int scancode, int action, int mods);

	int key = 0;
	int scancode = 0;
	int action = 0;
	int mods = 0;
};

#endif
