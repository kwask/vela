#ifndef MESH_H
#define MESH_H

#include <vector>
#include <map>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "vertex.h"
#include "shader.h"
#include "edge.h"

class Mesh
{
public:
	Mesh(Shader* shader = nullptr, GLenum render_mode = GL_TRIANGLES);
	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, Shader* shader, GLenum render_mode);

	GLuint triangleCount() const;

	void setShader(Shader* shader);
	void setRenderMode(GLenum render_mode);
	void init();
	void draw(const glm::mat4 &view, const glm::mat4 &projection, const glm::vec3 &position = glm::vec3(0.f, 0.f, 0.f), const double &size = 1.f, const double &scale = 1000.f, const double &luminosity = 0.f);
	void clear();
	void circle();
	void ellipses(double semimajor, double semiminor);
	void sphere();
private:
	void addTriangle(GLuint a, GLuint b, GLuint c);
	void addQuad(GLuint a, GLuint b, GLuint c, GLuint d);
	void calcNormals();
	void subdivideMesh();

	uint32_t subdivideEdge(uint32_t f0, uint32_t f1, const Vertex &v0, const Vertex &v1, Mesh &mesh, std::map<Edge, uint32_t> &divisions);

	glm::vec3 calcFaceNormal(const Vertex &v0, const Vertex &v1, const Vertex &v2);

	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	Shader* shader = nullptr;

	GLenum render_mode  = GL_TRIANGLES;

	GLuint vao = 0;
	GLuint vbo = 0;
	GLuint ebo = 0;

	GLint size_uni = -1;
	GLint scale_uni = -1;
	GLint model_uni = -1;
	GLint view_uni = -1;
	GLint project_uni = -1;
	GLint luminosity_uni = -1;
};

#endif
