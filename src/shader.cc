#include <iostream>
#include <GL/glew.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "shader.h"
#include "glhelpers.h"

namespace filesystem = boost::filesystem;

Shader::Shader()
{

}

Shader::~Shader()
{
	close();
}

void Shader::activate()
{
	glUseProgram(program);
	GLHelpers::checkError("shader activate");
}

void Shader::close()
{
	glDeleteShader(program);
}

GLuint Shader::getProgram()
{
	return program;
}

void Shader::loadProgram(filesystem::path vertex_path, filesystem::path frag_path, filesystem::path geometry_path)
{
	if(program != 0)
	{
		glDeleteProgram(program);
		program = 0;
	}

	program = glCreateProgram();

	if(program == 0)
	{
		std::printf("error creating program");
		return;
	}

	// Create the shaders
	GLuint vertex_shader = loadShader(vertex_path, GL_VERTEX_SHADER);
	GLuint frag_shader = loadShader(frag_path, GL_FRAGMENT_SHADER);
	GLuint geo_shader = loadShader(geometry_path, GL_GEOMETRY_SHADER);

	// Linking the program
	glAttachShader(program, vertex_shader);
	glAttachShader(program, frag_shader);
	if(geo_shader)
	{
		glAttachShader(program, geo_shader);
	}
	glLinkProgram(program);

	// Checking the program
	GLint result = GL_FALSE;
	int info_log_length = 0;

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
	if(result != GL_TRUE)
	{
		GLchar error_mess[info_log_length+1];
		glGetProgramInfoLog(program, info_log_length, 0, error_mess);
		std::printf("%s\n", &error_mess[0]);
	}

	// Cleaning up
	glDetachShader(program, vertex_shader);
	glDeleteShader(vertex_shader);

	glDetachShader(program, frag_shader);
	glDeleteShader(frag_shader);

	if(geo_shader)
	{
		glDetachShader(program, geo_shader);
		glDeleteShader(geo_shader);
	}
}

GLuint Shader::loadShader(filesystem::path path, GLuint type)
{
	if(path.string() == "")
	{
		return 0; // No error, shader was not specified
	}

	// Read the vertex shader code from the file
	if(!filesystem::exists(path))
	{
		std::printf("%s does not exist\n", path.string().c_str());
		return 0;
	}

	if(!filesystem::is_regular_file(path))
	{
		std::printf("%s is not a regular file\n", path.string().c_str());
		return 0;
	}

	GLuint shader_id = glCreateShader(type);

	std::string code;

	std::printf("compiling %s\n", path.string().c_str());
	filesystem::ifstream file;
	file.open(path);
	while(!file.eof())
	{
		std::string line = "";
		getline(file, line);
		code += line + "\n";
	}
	file.close();

	GLint result = GL_FALSE;
	int info_log_length = 0;

	// Compiling shader
	const char* code_pointer = code.c_str();
	glShaderSource(shader_id, 1, &code_pointer, 0);
	glCompileShader(shader_id);

	// Checking shader
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);
	if(result != GL_TRUE)
	{
		std::vector<char> error_mess(info_log_length+1);
		glGetShaderInfoLog(shader_id, info_log_length, 0, &error_mess[0]);
		std::printf("%s\n", &error_mess[0]);
	}

	return shader_id;
}
