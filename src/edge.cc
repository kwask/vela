#include <cstdint>

#include "edge.h"

Edge::Edge(uint32_t v0, uint32_t v1)
	: v0(v0 < v1 ? v0 : v1)
	, v1(v0 < v1 ? v1 : v0) {}

bool Edge::operator <(const Edge &rhs) const
{
	return v0 < rhs.v0 || (v0 == rhs.v0 && v1 < rhs.v1);
}
