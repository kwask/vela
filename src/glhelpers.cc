#include <cmath>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <CEGUI/System.h>

#include "universe.h"
#include "glhelpers.h"
#include "vertex.h"
#include "renderer.h"

void GLHelpers::windowResize(GLFWwindow* window, int length, int height)
{
	Renderer::window_length = length;
	Renderer::window_height = height;

	glfwMakeContextCurrent(window);

	glViewport(0, 0, (GLfloat)length, (GLfloat)height);

	// Needs this or else viewport is reset to original size when GUI is drawn
	if(CEGUI::System::getSingletonPtr() != nullptr)
	{
		CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Sizef(length, height));
	}
}

void GLHelpers::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Universe* universe = (Universe*)glfwGetWindowUserPointer(window);

	if(universe == nullptr)
	{
		std::printf("no universe");
		return;
	}

	universe->createKeypress(key, scancode, action, mods);
}

void GLHelpers::mouseCallback(GLFWwindow* window, double x, double y)
{
	Universe* universe = (Universe*)glfwGetWindowUserPointer(window);

	if(universe == nullptr)
	{
		std::printf("no universe");
		return;
	}

	Mouse &mouse = universe->mouse;

	if(mouse.initial_move)
	{
		mouse.initial_move = false;
		mouse.last_x = x;
		mouse.last_y = y;
	}

	mouse.x_offset = mouse.last_x-x;
	mouse.y_offset = mouse.last_y-y;

	mouse.last_x = x;
	mouse.last_y = y;

	if(mouse.update_current)
	{
		mouse.current_x = x;
		mouse.current_y = y;
	}
}

void GLHelpers::mousepressCallback(GLFWwindow* window, int key, int action, int mods)
{
	Universe* universe = (Universe*)glfwGetWindowUserPointer(window);

	if(universe == nullptr)
	{
		std::printf("no universe");
		return;
	}

	universe->createMousepress(key, action, mods);
}

void GLHelpers::scrollCallback(GLFWwindow* window, double x_offset, double y_offset)
{
	Universe* universe = (Universe*)glfwGetWindowUserPointer(window);

	if(universe == nullptr)
	{
		std::printf("no universe");
		return;
	}

	Mouse &mouse = universe->mouse;

	mouse.x_scroll_offset -= x_offset;
	mouse.y_scroll_offset -= y_offset;
}

void GLHelpers::errorCallback( int error, const char* description )
{
	fputs( description, stderr );
}

void GLHelpers::checkError(std::string name)
{
	GLenum error = glGetError();
	if(error != 0)
	{
		do
		{
			if(name != "")
			{
				std::printf("%s error %s: %d\n", glGetString(GL_VERSION), name.c_str(), error);
			} else {
				std::printf("%s error: %d\n", glGetString(GL_VERSION), error);
			}
			error = glGetError();
		} while (error != 0);
		std::getchar();
	}
}
