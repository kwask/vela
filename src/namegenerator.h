#ifndef NAMEGENERATOR_H
#define NAMEGENERATOR_H

#include <string>
#include <vector>

class NameGenerator
{
public:
	std::string genName();
	std::string genSerial(uint32_t length = 5);

	void setSyllables(std::vector<std::string> syllables);
	void setVowels(std::vector<std::string> vowels);

private:
	const static uint8_t SERIAL_LETTER_CHANCE = 30;

	std::vector<std::string> syllables;
	std::vector<std::string> vowels;

	bool has_serial = false;
};

#endif
