#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <CEGUI/System.h>
#include <CEGUI/GUIContext.h>
#include <CEGUI/MouseCursor.h>

#include "glhelpers.h"
#include "input.h"
#include "renderer.h"
#include "keypress.h"
#include "universe.h"
#include "camera.h"

Input::Input(Universe* universe)
	: Process(universe) {}

void Input::initCallbacks()
{
	std::printf("setting up callbacks\n");

	GLFWwindow* window = universe()->window;

	if(window == nullptr)
	{
		std::printf("could not set up callbacks, window was nullptr");
		return;
	}

	glfwSetKeyCallback(window, GLHelpers::keyCallback);

	glfwSetCursorPosCallback(window, GLHelpers::mouseCallback);
	glfwSetMouseButtonCallback(window, GLHelpers::mousepressCallback);
	glfwSetScrollCallback(window, GLHelpers::scrollCallback);
}

void Input::initKeybindings()
{
	std::printf("setting up mouse and key bindings\n");

	keybindings[GLFW_KEY_W] = Command::MOVE_FORWARD;
	keybindings[GLFW_KEY_S] = Command::MOVE_BACKWARD;
	keybindings[GLFW_KEY_A] = Command::MOVE_LEFT;
	keybindings[GLFW_KEY_D] = Command::MOVE_RIGHT;
	keybindings[GLFW_KEY_ESCAPE] = Command::EXIT;
	keybindings[GLFW_KEY_SPACE] = Command::PAUSE;

	mousebindings[GLFW_MOUSE_BUTTON_1] = Command::SCREEN_GRAB;
}

void Input::init()
{
	std::printf("INPUT INIT\n");

	initCallbacks();
	initKeybindings();

	GLHelpers::checkError("input init");
}

void Input::process()
{
	glfwPollEvents();
	processKeyboard();
	processMouse();

	updateProcessTime();
	GLHelpers::checkError("input");
	return;
}

void Input::close()
{
	std::printf("INPUT CLOSE\n");
}

void Input::processMouse()
{
	Mouse& mouse = universe()->mouse;
	mouse.update_current = !screen_grabbed; // If screen is grabbed, do not update visual mouse position

	CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

	if(!screen_grabbed)
	{
		context.injectMousePosition(mouse.current_x, mouse.current_y);
	}

	for(
		uint32_t i = 0;
		i <= MAX_COMMAND_PROCESS_PER_IT && !universe()->mousepress_stream.empty();
		i++)
	{
		Keypress* mousepress = universe()->mousepress_stream.front();
		if(mousepress == nullptr)
		{
			continue;
		}
		processMousepress(mousepress);

		universe()->mousepress_stream.pop();
		delete mousepress;
	}

	Camera* cam = universe()->active_camera;

	// Movement process
	if(screen_grabbed)
	{
		cam->yaw += mouse.x_offset*cam->mouse_sensitivity;
		cam->pitch += mouse.y_offset*cam->mouse_sensitivity;

		mouse.x_offset = 0;
		mouse.y_offset = 0;

		if(cam->PITCH_LIMIT)
		{
			if(cam->pitch > cam->PITCH_LIMIT)
			{
				cam->pitch = cam->PITCH_LIMIT;
			}else if(cam->pitch < -cam->PITCH_LIMIT)
			{
				cam->pitch = -cam->PITCH_LIMIT;
			}
		}
	}

	// Scroll process
	if(!context.injectMouseWheelChange(mouse.y_scroll_offset))
	{
		double distance_diff = cam->distance*mouse.y_scroll_offset*cam->zoom_sensitivity;
		cam->distance += distance_diff;

		if(cam->distance > cam->DISTANCE_LIMIT)
		{
			cam->distance = cam->DISTANCE_LIMIT;
		}
	}

	mouse.x_scroll_offset = 0;
	mouse.y_scroll_offset = 0;
}

void Input::processMousepress(Keypress* mousepress)
{
	if(mousepress == nullptr)
	{
		return;
	}

	CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::MouseCursor& cursor = context.getMouseCursor();

	GLFWwindow* window = universe()->window;

	switch(mousepress->action)
	{
	case GLFW_PRESS:
		if(context.injectMouseButtonDown(glfwToCEGUIMouse(mousepress->key)))
		{
			return;
		}

		switch(mousebindings[mousepress->key])
		{
		case Command::SCREEN_GRAB:
			screen_grabbed = true;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			cursor.hide();
			break;
		}
		break;
	case GLFW_RELEASE:
		if(context.injectMouseButtonUp(glfwToCEGUIMouse(mousepress->key)))
		{
			return;
		}

		switch(mousebindings[mousepress->key])
		{
		case Command::SCREEN_GRAB:
			screen_grabbed = false;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			cursor.show();
			break;
		}
		break;
	}
}

void Input::processKeyboard()
{
	for(
		uint32_t i = 0;
		i <= MAX_COMMAND_PROCESS_PER_IT && !universe()->keypress_stream.empty();
		i++)
	{
		Keypress* keypress = universe()->keypress_stream.front();
		if(keypress == nullptr)
		{
			continue;
		}
		processKeypress(keypress);

		universe()->keypress_stream.pop();
		delete keypress;
	}

	Camera* cam = universe()->active_camera;
	GLfloat cam_velocity = (cam->speed*cam->distance*universe()->SCALE)/Renderer::frames_per_second;

	if(moving[Movement::FORWARD])
	{
		cam->target_pos += glm::vec3(cam->front.x, cam->front.y, 0.f)*cam_velocity;
	}

	if(moving[Movement::BACKWARD])
	{
		cam->target_pos -= glm::vec3(cam->front.x, cam->front.y, 0.f)*cam_velocity;
	}

	if(moving[Movement::LEFT])
	{
		cam->target_pos -= cam->right*cam_velocity;
	}

	if(moving[Movement::RIGHT])
	{
		cam->target_pos += cam->right*cam_velocity;
	}
}

void Input::processKeypress(Keypress* keypress)
{
	if(keypress == nullptr)
	{
		return;
	}

	CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

	switch(keypress->action)
	{
	case GLFW_PRESS:
		if(context.injectKeyDown(glfwToCEGUIKey(keypress->key)))
		{
			return;
		}

		switch(keybindings[keypress->key])
		{
		case Command::MOVE_FORWARD:
			moving[Movement::FORWARD] = true;
			break;
		case Command::MOVE_BACKWARD:
			moving[Movement::BACKWARD] = true;
			break;
		case Command::MOVE_LEFT:
			moving[Movement::LEFT] = true;
			break;
		case Command::MOVE_RIGHT:
			moving[Movement::RIGHT] = true;
			break;
		case Command::EXIT:
			universe()->exit = true;
			break;
		case Command::PAUSE:
			universe()->paused = !universe()->paused;
			break;
		}
		break;
	case GLFW_RELEASE:
		if(context.injectKeyUp(glfwToCEGUIKey(keypress->key)))
		{
			return;
		}

		switch(keybindings[keypress->key])
		{
		case Command::MOVE_FORWARD:
			moving[Movement::FORWARD] = false;
			break;
		case Command::MOVE_BACKWARD:
			moving[Movement::BACKWARD] = false;
			break;
		case Command::MOVE_LEFT:
			moving[Movement::LEFT] = false;
			break;
		case Command::MOVE_RIGHT:
			moving[Movement::RIGHT] = false;
			break;
		}
		break;
	}
}

CEGUI::Key::Scan Input::glfwToCEGUIKey(int key)
{
   switch (key) {
      case GLFW_KEY_UNKNOWN: return CEGUI::Key::Unknown;
      case GLFW_KEY_ESCAPE: return CEGUI::Key::Escape;
      case GLFW_KEY_F1: return CEGUI::Key::F1;
      case GLFW_KEY_F2: return CEGUI::Key::F2;
      case GLFW_KEY_F3: return CEGUI::Key::F3;
      case GLFW_KEY_F4: return CEGUI::Key::F4;
      case GLFW_KEY_F5: return CEGUI::Key::F5;
      case GLFW_KEY_F6: return CEGUI::Key::F6;
      case GLFW_KEY_F7: return CEGUI::Key::F7;
      case GLFW_KEY_F8: return CEGUI::Key::F8;
      case GLFW_KEY_F9: return CEGUI::Key::F9;
      case GLFW_KEY_F10: return CEGUI::Key::F10;
      case GLFW_KEY_F11: return CEGUI::Key::F11;
      case GLFW_KEY_F12: return CEGUI::Key::F12;
      case GLFW_KEY_F13: return CEGUI::Key::F13;
      case GLFW_KEY_F14: return CEGUI::Key::F14;
      case GLFW_KEY_F15: return CEGUI::Key::F15;
      case GLFW_KEY_UP: return CEGUI::Key::ArrowUp;
      case GLFW_KEY_DOWN: return CEGUI::Key::ArrowDown;
      case GLFW_KEY_LEFT: return CEGUI::Key::ArrowLeft;
      case GLFW_KEY_RIGHT: return CEGUI::Key::ArrowRight;
      case GLFW_KEY_LEFT_SHIFT: return CEGUI::Key::LeftShift;
      case GLFW_KEY_RIGHT_SHIFT: return CEGUI::Key::RightShift;
      case GLFW_KEY_LEFT_CONTROL: return CEGUI::Key::LeftControl;
      case GLFW_KEY_RIGHT_CONTROL: return CEGUI::Key::RightControl;
      case GLFW_KEY_LEFT_ALT: return CEGUI::Key::LeftAlt;
      case GLFW_KEY_RIGHT_ALT: return CEGUI::Key::RightAlt;
      case GLFW_KEY_TAB: return CEGUI::Key::Tab;
      case GLFW_KEY_ENTER: return CEGUI::Key::Return;
      case GLFW_KEY_BACKSPACE: return CEGUI::Key::Backspace;
      case GLFW_KEY_INSERT: return CEGUI::Key::Insert;
      case GLFW_KEY_DELETE: return CEGUI::Key::Delete;
      case GLFW_KEY_PAGE_UP: return CEGUI::Key::PageUp;
      case GLFW_KEY_PAGE_DOWN: return CEGUI::Key::PageDown;
      case GLFW_KEY_HOME: return CEGUI::Key::Home;
      case GLFW_KEY_END: return CEGUI::Key::End;
      case GLFW_KEY_NUM_LOCK: return CEGUI::Key::NumLock;
      case GLFW_KEY_KP_ENTER: return CEGUI::Key::NumpadEnter;
      case GLFW_KEY_A: return CEGUI::Key::A;
      case GLFW_KEY_B: return CEGUI::Key::B;
      case GLFW_KEY_C: return CEGUI::Key::C;
      case GLFW_KEY_D: return CEGUI::Key::D;
      case GLFW_KEY_E: return CEGUI::Key::E;
      case GLFW_KEY_F: return CEGUI::Key::F;
      case GLFW_KEY_G: return CEGUI::Key::G;
      case GLFW_KEY_H: return CEGUI::Key::H;
      case GLFW_KEY_I: return CEGUI::Key::I;
      case GLFW_KEY_J: return CEGUI::Key::J;
      case GLFW_KEY_K: return CEGUI::Key::K;
      case GLFW_KEY_L: return CEGUI::Key::L;
      case GLFW_KEY_M: return CEGUI::Key::M;
      /* Char Ñ in Spanish-Keyboard */
      case GLFW_KEY_SEMICOLON: return CEGUI::Key::Semicolon;
      case GLFW_KEY_N: return CEGUI::Key::N;
      case GLFW_KEY_O: return CEGUI::Key::O;
      case GLFW_KEY_P: return CEGUI::Key::P;
      case GLFW_KEY_Q: return CEGUI::Key::Q;
      case GLFW_KEY_R: return CEGUI::Key::R;
      case GLFW_KEY_S: return CEGUI::Key::S;
      case GLFW_KEY_T: return CEGUI::Key::T;
      case GLFW_KEY_U: return CEGUI::Key::U;
      case GLFW_KEY_V: return CEGUI::Key::V;
      case GLFW_KEY_W: return CEGUI::Key::W;
      case GLFW_KEY_X: return CEGUI::Key::X;
      case GLFW_KEY_Y: return CEGUI::Key::Y;
      case GLFW_KEY_Z: return CEGUI::Key::Z;
      case GLFW_KEY_0: return CEGUI::Key::Zero;
      case GLFW_KEY_1: return CEGUI::Key::One;
      case GLFW_KEY_2: return CEGUI::Key::Two;
      case GLFW_KEY_3: return CEGUI::Key::Three;
      case GLFW_KEY_4: return CEGUI::Key::Four;
      case GLFW_KEY_5: return CEGUI::Key::Five;
      case GLFW_KEY_6: return CEGUI::Key::Six;
      case GLFW_KEY_7: return CEGUI::Key::Seven;
      case GLFW_KEY_8: return CEGUI::Key::Eight;
      case GLFW_KEY_9: return CEGUI::Key::Nine;
      case GLFW_KEY_KP_0: return CEGUI::Key::Zero;
      case GLFW_KEY_KP_1: return CEGUI::Key::One;
      case GLFW_KEY_KP_2: return CEGUI::Key::Two;
      case GLFW_KEY_KP_3: return CEGUI::Key::Three;
      case GLFW_KEY_KP_4: return CEGUI::Key::Four;
      case GLFW_KEY_KP_5: return CEGUI::Key::Five;
      case GLFW_KEY_KP_6: return CEGUI::Key::Six;
      case GLFW_KEY_KP_7: return CEGUI::Key::Seven;
      case GLFW_KEY_KP_8: return CEGUI::Key::Eight;
      case GLFW_KEY_KP_9: return CEGUI::Key::Nine;
      case GLFW_KEY_KP_ADD: return CEGUI::Key::Add;
      case GLFW_KEY_KP_DECIMAL: return CEGUI::Key::Decimal;
      case GLFW_KEY_KP_DIVIDE: return CEGUI::Key::Divide;
      case GLFW_KEY_KP_EQUAL: return CEGUI::Key::NumpadEquals;
      case GLFW_KEY_KP_MULTIPLY: return CEGUI::Key::Multiply;
      case GLFW_KEY_KP_SUBTRACT: return CEGUI::Key::Subtract;
      case GLFW_KEY_APOSTROPHE: return CEGUI::Key::Apostrophe;
      case GLFW_KEY_COMMA: return CEGUI::Key::Comma;
      case GLFW_KEY_EQUAL: return CEGUI::Key::Equals;
      case GLFW_KEY_PERIOD: return CEGUI::Key::Period;
      case GLFW_KEY_GRAVE_ACCENT: return CEGUI::Key::Grave;
      case GLFW_KEY_LEFT_BRACKET: return CEGUI::Key::LeftBracket;
      case GLFW_KEY_BACKSLASH: return CEGUI::Key::Backslash;
      case GLFW_KEY_RIGHT_BRACKET: return CEGUI::Key::RightBracket;
      case GLFW_KEY_MINUS: return CEGUI::Key::Minus;
      case GLFW_KEY_SLASH: return CEGUI::Key::Slash;
      case GLFW_KEY_SPACE: return CEGUI::Key::Space;
      case GLFW_KEY_WORLD_1: return CEGUI::Key::Unknown;
      case GLFW_KEY_WORLD_2: return CEGUI::Key::Unknown;

      default: return CEGUI::Key::Unknown;
   }
}

CEGUI::MouseButton Input::glfwToCEGUIMouse(int button)
{
	switch(button)
	{
		case GLFW_MOUSE_BUTTON_LEFT	: return CEGUI::LeftButton;
		case GLFW_MOUSE_BUTTON_RIGHT	: return CEGUI::RightButton;
		case GLFW_MOUSE_BUTTON_MIDDLE	: return CEGUI::MiddleButton;
		default				: return CEGUI::NoButton;
	}
}
