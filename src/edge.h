#ifndef EDGE_H
#define EDGE_H

#include <cstdint>

struct Edge
{
	uint32_t v0;
	uint32_t v1;

	Edge(uint32_t v0, uint32_t v1);

	bool operator <(const Edge &rhs) const;
};

#endif
