#ifndef INPUT_H
#define INPUT_H

#include <CEGUI/GUIContext.h>

#include "process.h"
#include "keypress.h"

namespace Movement
{
	const unsigned char FORWARD = 0;
	const unsigned char LEFT = 1;
	const unsigned char RIGHT = 2;
	const unsigned char BACKWARD = 3;
	const unsigned char MOVE_DIRS = 4;
}

namespace Command
{
	const uint32_t NONE = 0;
	const uint32_t EXIT = 1;
	const uint32_t MOVE_FORWARD = 2;
	const uint32_t MOVE_BACKWARD = 3;
	const uint32_t MOVE_LEFT = 4;
	const uint32_t MOVE_RIGHT = 5;
	const uint32_t PAUSE = 6;
	const uint32_t SCREEN_GRAB = 7;
}

class Input : public Process
{
public:
	Input(Universe* universe);

	void init() override;
	void process() override;
	void close() override;

	// The bindings
	uint32_t keybindings[512] = {Command::NONE};
	uint32_t mousebindings[512] = {Command::NONE};

private:
	bool moving[Movement::MOVE_DIRS] = {false, false, false, false};
	bool screen_grabbed = false;

	static const uint32_t MAX_COMMAND_PROCESS_PER_IT = 256;

	void initCallbacks();
	void initKeybindings();

	void processMouse();
	void processMousepress(Keypress* mousepress);
	void processKeyboard();
	void processKeypress(Keypress* keypress);

	CEGUI::Key::Scan glfwToCEGUIKey(int key);
	CEGUI::MouseButton glfwToCEGUIMouse(int button);
};

#endif
