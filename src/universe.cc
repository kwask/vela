#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "universe.h"
#include "shader.h"

namespace filesystem = boost::filesystem;

Universe::~Universe()
{
	close();
}

void Universe::close()
{
	std::printf("UNIVERSE CLOSE\n");

	std::printf("deleting %lu solar systems\n", solarsystems.size());
	for(auto it : solarsystems)
	{
		delete it.second;
	}

	std::printf("deleting %lu astrobodies\n", astrobodies.size());
	for(auto it : astrobodies)
	{
		delete it.second;
	}

	std::printf("deleting %lu lifeforms\n", life_types.size());
	for(auto it : life_types)
	{
		delete it.second;
	}

	std::printf("deleting %lu materials\n", materials.size());
	for(auto it : materials)
	{
		delete it.second;
	}

	std::printf("deleting %lu keypresses\n", keypress_stream.size());
	while(!keypress_stream.empty())
	{
		std::printf("deleting keypress\n");
		Keypress* keypress = keypress_stream.front();
		keypress_stream.pop();
		delete keypress;
	}

	std::printf("deleting %lu mousepresses\n", mousepress_stream.size());
	while(!keypress_stream.empty())
	{
		std::printf("deleting mousepress\n");
		Keypress* mousepress = mousepress_stream.front();
		mousepress_stream.pop();
		delete mousepress;
	}

	std::printf("deleting %lu shaders\n", shaders.size());
	for(auto it : shaders)
	{
		glDeleteProgram(it.second->getProgram());
		delete it.second;
	}
}

void Universe::loadShader(std::string name, filesystem::path vertex_path, filesystem::path frag_path, filesystem::path geometry_path)
{
	if(name == "")
	{
		std::printf("name not given for shader");
		return;
	}

	Shader* shader = new Shader;
	shader->loadProgram(vertex_path, frag_path, geometry_path);
	if(shader->getProgram() == 0)
	{
		std::printf("shader program was 0");
		return;
	}

	shaders.insert(std::make_pair(name, shader));
}

Astrobody* Universe::createAstrobody(double mass, double radius, double true_anomaly, std::string name, Physical* primary, double luminosity)
{
	Physical phys(mass, radius, true_anomaly, luminosity);
	phys.primary = primary;

	Astrobody* astro = new Astrobody(astrobody_count.increment(), phys);
	astro->setName(name);

	if(phys.getOrbitRadius() > 0)
	{
		astro->physical.orbit = &circle_mesh;
	}

	astro->physical.object = &sphere_mesh;

	astrobodies.insert(std::make_pair(astro->getName(), astro));
	return astro;
}

Material* Universe::createMaterial(instruct_set* instructions)
{
	Material* material = new Material(material_count.increment());

	material->parse(instructions);

	materials.insert(std::make_pair(material->getName(), material));
	return material;
}

SolarSystem* Universe::createSolarSystem(std::string name)
{
	if(name.length() == 0)
	{
		name = "brownie";
	}

	SolarSystem* solarsystem = new SolarSystem(solarsystem_count.increment());
	solarsystem->setName(name);

	solarsystems.insert(std::make_pair(name, solarsystem));

	return solarsystem;
}

Keypress* Universe::createKeypress(int key, int scancode, int action, int mods)
{
	Keypress* k = new Keypress(key, scancode, action, mods);
	keypress_stream.push(k);

	return k;
}


Keypress* Universe::createMousepress(int button, int action, int mods)
{
	Keypress* m = new Keypress(button, 0, action, mods);
	mousepress_stream.push(m);

	return m;
}
