#include <cstdlib>

#include "namegenerator.h"

std::string NameGenerator::genName()
{
	std::string name = "";

	name += syllables[rand()%syllables.size()];
	name += vowels[rand()%vowels.size()];
	name += syllables[rand()%syllables.size()];

	name += " "+genSerial();

	name[0] = toupper(name[0]);

	return name;
}

std::string NameGenerator::genSerial(uint32_t length)
{
	std::string number = "";

	for(; number.length() < length;)
	{
		if(rand()%(100/SERIAL_LETTER_CHANCE)==0)
		{
			number += (char)('A'+rand()%26);
		}else
		{
			number += (char)('0'+rand()%10);
		}
	}

	return number;
}

void NameGenerator::setSyllables(std::vector<std::string> syllables)
{
	this->syllables = syllables;
}

void NameGenerator::setVowels(std::vector<std::string> vowels)
{
	this->vowels = vowels;
}
