#include <queue>
#include <string>

#include "basic.h"

Basic::Basic(uint64_t identifier)
	: identifier(identifier) {}

Basic::~Basic()
{

}

void Basic::setName(const std::string name)
{
	this->name = name;
}

uint64_t Basic::ident()
{
	return identifier;
}

std::string Basic::getName() const
{
	return name;
}
