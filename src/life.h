#ifndef LIFE_H
#define LIFE_H

#include <queue>
#include <string>

#include "basic.h"

// Life covers any form of life
class Life : public Basic
{
public:
	Life(uint64_t identifier);

	void parse(instruct_set* instructions) override;

private:

};

#endif
