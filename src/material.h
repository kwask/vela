#ifndef MATERIAL_H
#define MATERIAL_H

#include <string>
#include <map>
#include <queue>

#include "basic.h"

namespace State
{
	const unsigned char SOLID = 0;
	const unsigned char LIQUID = 1;
	const unsigned char GAS = 2;
	const unsigned char PLASMA = 3;
	const unsigned char TOTAL = 4;
}

class Material : public Basic
{
public:
	Material(uint64_t identifier);
	~Material();

	void parse(instruct_set* instructions) override;

	std::string getName();

private:
	double density = 0;
	double state_temps[State::TOTAL];

	std::string name; // Common name, should be unique
	std::string color;
	std::string state_names[State::TOTAL]; // Unique state names

	// TODO this should be a map or something

};

#endif
