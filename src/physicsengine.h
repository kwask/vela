#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H

#include "process.h"
#include "universe.h"

class PhysicsEngine : protected Process
{
public:
	PhysicsEngine(Universe* universe);

	void init() override;
	void process() override;
	void close() override;
private:

};

#endif
