#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "process.h"

Process::Process(Universe* universe)
	: univ(universe) {}

Universe* Process::universe()
{
	return univ;
}

void Process::setUniverse(Universe* universe)
{
	this->univ = universe;
}

void Process::updateProcessTime()
{
	last_process = glfwGetTime();
}

double Process::getLastProcessTime() const
{
	return last_process;
}
