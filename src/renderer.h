#ifndef RENDERER_H
#define RENDERER_H

#include <float.h>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include "process.h"
#include "shader.h"
#include "universe.h"

class Renderer : public Process
{
public:
	static constexpr double FOV = 65.f;
	static constexpr double NEAR = 1.f;
	static constexpr double FAR = FLT_MAX;

	Renderer(Universe* universe);

	void init() override;
	void process() override;
	void close() override;

	static double window_height;
	static double window_length;
	static unsigned char frames_per_second;
private:
	void initGLFW();
	void initShaders();
	void initMeshes();
	void initGUI();
	void render3DScene();
	void renderGUI();

	CEGUI::OpenGL3Renderer* gui_renderer = nullptr;
};

#endif
