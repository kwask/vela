#include <iostream>
#include <cstdlib>
#include <fstream>
#include <queue>
#include <string>
#include <cstring>
#include <iterator>
#include <vector>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/algorithm/string.hpp>

#include "generator.h"
#include "astrobody.h"
#include "material.h"
#include "life.h"
#include "glhelpers.h"

namespace filesystem = boost::filesystem;

Generator::Generator(Universe* universe)
	: Process(universe) {}

// Sets up the universe
void Generator::init()
{
	std::printf("GENERATOR INIT\n");

	/* TODO
	1. Generate nature
	2. Get premade Material files
	3. Parse premade Material files, add results to universe
	4. Link materials
	5. Get premade SolarSystem files
	6. Parse premade SolarSystem files, add results to universe
	7. Get premade Life files
	8. Parse premade Life files
	9. Create home systems for Life, add resulting creatures and planets to universe
	*/

	initNameGens();
	initSolarSystems();
	//parseDir(material_dir);
}

void Generator::process()
{
	/* TODO
	1. Determine if a new creature / civilization should be added
	2. Determine if the nature of reality should be expanded
	*/

	updateProcessTime();
}

void Generator::close()
{
	std::printf("GENERATOR CLOSE\n");
}

void Generator::parseDir(filesystem::path dir)
{
	std::printf("parsing %s\n", dir.string().c_str());

	try
	{
		if(!filesystem::exists(dir))
		{
			std::printf("directory does not exist\n");
			return;
		}

		if(!filesystem::is_directory(dir))
		{
			std::printf("path is not a directory\n");
			return;
		}

		// Get all of the paths in this directory
		std::vector<filesystem::path> paths;
		copy(filesystem::directory_iterator(dir),
			 filesystem::directory_iterator(),
			 back_inserter(paths));

		for(auto d : paths)
		{
			std::printf("loading %s\n", d.string().c_str());

			instruct_set* instructions = read(d);
			if(instructions != nullptr)
			{
				parse(instructions);
			}
		}
	}
	catch (const filesystem::filesystem_error &ex)
	{
		std::printf("%s\n", ex.what());
	}
}

instruct_set* Generator::read(filesystem::path dir)
{
	if(!filesystem::exists(dir))
	{
		std::printf("file does not exist\n");
		return nullptr;
	}

	if(!is_regular_file(dir))
	{
		std::printf("path is not a regular file\n");
		return nullptr;
	}

	filesystem::ifstream file;
	file.open(dir);

	instruct_set* instructions = new(instruct_set);

	typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
	boost::char_separator<char> delim{":\t\n"};

	for(uint32_t i = 0;
		i < Generator::MAX_FILE_LINES && !file.eof();
		i++)
	{
		// Getting the next line to tokenize
		char line[Generator::MAX_LINE_SIZE];
		file.getline(line, Generator::MAX_LINE_SIZE);
		std::string s(line);
		std::printf("> %s\n", s.c_str());
		boost::erase_all(s, "\t"); // Tabs are dropped

		// Splitting each line of the file into individual tokens
		tokenizer tokens{s, delim};
		for(const auto &token : tokens)
		{
			if(instructions->size() >= Generator::MAX_INSTRUCTIONS)
			{
				file.close();
				return instructions;
			}

			instructions->push(token);
		}
	}

	file.close();
	return instructions;
}

void Generator::parse(instruct_set* instructions)
{
	if(instructions == nullptr)
	{
		return;
	}

	for(uint32_t i = 0;
		i < Generator::MAX_INSTRUCTIONS && !instructions->empty();
		i++)
	{
		std::string command = instructions->front();
		instructions->pop();

		// TODO Turn this into switch statement using mapped integers
		if(command == "NEW_LIFE") {
			// TODO create life object and pass instructions to it
		}else if(command == "NEW_MATERIAL") {
			std::printf("adding new material\n");
			universe()->createMaterial(instructions);
			std::printf("material added\n");
		}else if(command == "NEW_ASTROBODY") {
			// TODO create astrobody object and pass instruction to it
		}
	}
}

void Generator::initSolarSystems()
{
	std::printf("populating solar system\n");

	Astrobody* sun = universe()->createAstrobody(1.9891e+27, 0, 0, "sun", nullptr, 1.f);

	double orbit = sun->physical.getBodyRadius();

	// TEMPORARY, adding for visuals
	for(int i = 0; i<10; i++)
	{
		orbit += 1.496E+8f;

		std::string name = name_gen.genName();

		Astrobody* planet = universe()->createAstrobody(
			5.972E+21f,
			orbit,
			rand()%7,
			name,
			&sun->physical);

		int moons = 10;
		double moon_orbit = planet->physical.getBodyRadius();
		moons += rand()%3;
		if(rand()%2 == 0)
		{
			moons += rand()%4;

			if(rand()%2 == 0)
			{
				moons += rand()%8;
			}
		}

		for(int j = 0; j<moons; j++)
		{
			double last_orbit = moon_orbit;
			moon_orbit += 384400.f;

			if(last_orbit == moon_orbit && j > 0)
			{
				continue;
			}

			name = name_gen.genName();

			universe()->createAstrobody(
				7.34767309E+19f,
				moon_orbit,
				rand()%7,
				name,
				&planet->physical);
		}
	}

	universe()->createAstrobody(
		5.972E+21f,
		3.67E+9f,
		rand()%7,
		"pluto",
		&sun->physical);
}

void Generator::initNameGens()
{
	std::printf("populating name gens\n");

	std::string consonants_path = name_dir+"/consonants.txt";
	std::string vowels_path = name_dir+"/vowels.txt";

	filesystem::ifstream file;
	file.open(consonants_path);
	std::vector<std::string> consonants;

	while(!file.eof())
	{
		char line[Generator::MAX_LINE_SIZE];
		file.getline(line, Generator::MAX_LINE_SIZE);
		std::string s(line);

		if(s.length() > 0)
		{
			consonants.emplace_back(s);
		}
	}
	file.close();

	name_gen.setSyllables(consonants);

	file.open(vowels_path);
	std::vector<std::string> vowels;

	while(!file.eof())
	{
		char line[Generator::MAX_LINE_SIZE];
		file.getline(line, Generator::MAX_LINE_SIZE);
		std::string s(line);

		if(s.length() > 0)
		{
			vowels.emplace_back(s);
		}
	}
	file.close();

	name_gen.setVowels(vowels);
}
