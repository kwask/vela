#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace filesystem = boost::filesystem;

class Shader
{
public:

	Shader();
	~Shader();

	void activate();
	void close();
	void loadProgram(filesystem::path vertex_path, filesystem::path frag_path, filesystem::path geometry_path = "");

	GLuint getProgram();
private:

	GLuint loadShader(filesystem::path path, GLuint type);
	GLuint program = 0;
};

#endif
