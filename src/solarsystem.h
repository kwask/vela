#ifndef SOLARSYSTEM_H
#define SOLARSYSTEM_H

#include <map>
#include <string>

#include "basic.h"
#include "astrobody.h"

class SolarSystem : public Basic
{
public:
	SolarSystem(uint64_t identifier);

	void close();
	void parse(instruct_set* instructions) override;

private:
	std::string name;

	double x_pos = 0; // Galactic x position
	double y_pos = 0; // Galactic y position

	std::map<std::string, Astrobody*> astrobodies;
};

#endif
