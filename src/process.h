#ifndef PROCESS_H
#define PROCESS_H

#include "universe.h"

class Process
{
public:
	Process(Universe* universe);

	// Gets universe, shortened because its annoying to use everywhere
	Universe* universe();

	void setUniverse(Universe* universe);
	void updateProcessTime();

	double getLastProcessTime() const;

	virtual void init() = 0;
	virtual void process() = 0;
	virtual void close() = 0;

private:
	Universe* univ = nullptr;

	double last_process = 0;
};

#endif
