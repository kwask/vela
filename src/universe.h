#ifndef UNIVERSE_H
#define UNIVERSE_H

#include <map>
#include <queue>
#include <string>

#include "camera.h"
#include "keypress.h"
#include "mouse.h"
#include "material.h"
#include "astrobody.h"
#include "solarsystem.h"
#include "life.h"
#include "solarsystem.h"
#include "counter.h"
#include "namegenerator.h"

struct Universe
{
	~Universe();

	void close();
	void loadShader(std::string name, filesystem::path vertex_path, filesystem::path frag_path, filesystem::path geometry_path = "");

	Astrobody* createAstrobody(double mass, double radius, double true_anomaly, std::string name, Physical* primary = nullptr, double luminosity = 0.f);
	Material* createMaterial(instruct_set* instructions);
	SolarSystem* createSolarSystem(std::string name);
	Life* createLife();
	Keypress* createKeypress(int key, int scancode, int action, int mods);
	Keypress* createMousepress(int button, int action, int mods);

	// Setting this to false stops the game
	bool exit = false;
	bool paused = false;

	// Cameras
	Camera* active_camera = nullptr; // the current active camera
	Camera system_camera; // Camera settings for in-system viewing
	Camera galaxy_camera; // Camera settings for galaxy-wide viewing

	// GLFW
	GLFWwindow* window = nullptr;

	// Indexer
	Counter creature_count;
	Counter astrobody_count;
	Counter material_count;
	Counter solarsystem_count;

	// Active solar system
	SolarSystem* active_solarsystem = nullptr;

	// Universe objects
	std::map<std::string, Material*> materials;
	std::map<std::string, Astrobody*> astrobodies;
	std::map<std::string, SolarSystem*> solarsystems;
	std::map<std::string, Life*> life_types;

	// User input
	std::queue<Keypress*> keypress_stream;
	std::queue<Keypress*> mousepress_stream;
	Mouse mouse;

	// Shaders
	std::map<std::string, Shader*> shaders;

	// Models
	Mesh sphere_mesh;
	Mesh circle_mesh;

	// TODO make this seconds per second
	double timescale = 1440.f; // How many seconds per tick
	static constexpr double SCALE = 1.f/1E+3f; // Scale multiplier, a value of 1 means 1 GLunit = 1 km
};

#endif
