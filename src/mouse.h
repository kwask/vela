#ifndef MOUSE_H
#define MOUSE_H

#include <GL/glew.h>

struct Mouse
{
	// These two are used for visual cursor position
	double current_x = 0;
	double current_y = 0;

	double last_x = 0;
	double last_y = 0;

	double x_offset = 0;
	double y_offset = 0;

	double x_scroll_offset = 0;
	double y_scroll_offset = 0;

	bool initial_move = true;
	bool update_current = true;
};

#endif
