#include <iostream>
#include <cstdlib>
#include <string>

#include "material.h"
#include "generator.h"

Material::Material(uint64_t identifier)
	: Basic(identifier){}

Material::~Material()
{

}

void Material::parse(instruct_set* instructions)
{
	if(instructions == nullptr)
	{
		return;
	}

	for(unsigned int i = 0;
		i < Generator::MAX_INSTRUCTIONS && !instructions->empty();
		i++)
	{
		std::string command = instructions->front();
		instructions->pop();

		// TODO Turn this into switch statement using mapped integers
		if(command == "NAME") {
			name = instructions->front();
			instructions->pop();

			std::printf("name: %s\n", name.c_str());
		}else if(command == "COLOR") {
			color = instructions->front();
			instructions->pop();

			std::printf("color: %s\n", color.c_str());
		}else if(command == "DENSITY") {
			density = atof(instructions->front().c_str());
			instructions->pop();

			std::printf("density: %E\n", density);
		}else if(command == "TEMP_RANGES") {
			state_temps[State::SOLID] = atof(instructions->front().c_str());
			instructions->pop();

			state_temps[State::LIQUID] = atof(instructions->front().c_str());
			instructions->pop();

			state_temps[State::GAS] = atof(instructions->front().c_str());
			instructions->pop();

			state_temps[State::PLASMA] = atof(instructions->front().c_str());
			instructions->pop();

			std::printf("state temps\n\tsolid: %F\n\tliquid: %F\n\tgas: %F\n\tplasma: %F\n",
				state_temps[State::SOLID],
				state_temps[State::LIQUID],
				state_temps[State::GAS],
				state_temps[State::PLASMA]);
		}else if(command == "STATE_NAMES") {
			state_names[State::SOLID] = instructions->front().c_str();
			instructions->pop();

			state_names[State::LIQUID] = instructions->front().c_str();
			instructions->pop();

			state_names[State::GAS] = instructions->front().c_str();
			instructions->pop();

			state_names[State::PLASMA] = instructions->front().c_str();
			instructions->pop();

			std::printf("state names\n\tsolid: %s\n\tliquid: %s\n\tgas: %s\n\tplasma: %s\n",
				state_names[State::SOLID].c_str(),
				state_names[State::LIQUID].c_str(),
				state_names[State::GAS].c_str(),
				state_names[State::PLASMA].c_str());
		}else if(command == "ADD_MATERIAL") {
			// TODO add materials
			std::printf("adding material");
		}else if(command == "END_MATERIAL") {
			std::printf("finishing material");
			return;
		}
	}
}

std::string Material::getName()
{
	return name;
}
