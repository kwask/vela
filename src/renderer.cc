#include <iostream>
#include <vector>
#include <map>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include <CEGUI/System.h>
#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/GUIContext.h>
#include <CEGUI/SchemeManager.h>
#include <CEGUI/FontManager.h>
#include <CEGUI/ImageManager.h>
#include <CEGUI/ScriptModule.h>
#include "CEGUI/falagard/WidgetLookManager.h"
#include <CEGUI/DefaultResourceProvider.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include "shader.h"
#include "renderer.h"
#include "glhelpers.h"

unsigned char Renderer::frames_per_second = 60;

double Renderer::window_length = 1024;
double Renderer::window_height = 600;

Renderer::Renderer(Universe* universe)
	: Process(universe) {}

void Renderer::initGLFW()
{
	std::printf("initializing GLFW\n");

	glfwInit();
	glfwSetErrorCallback(GLHelpers::errorCallback);

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // We want OpenGL 4.4
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE); // This is needed to work with GLEW
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); doesn't work with GLEW

	// Creating and setting our window as current context
	GLFWwindow* window = glfwCreateWindow(window_length, window_height, "Vela", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	universe()->window = window;

	glewExperimental = true;
	if(glewInit() != GLEW_OK)
	{
		std::printf("GLEW failed to init\n");
		close();
	}

	glfwSetWindowSizeCallback(window, GLHelpers::windowResize);
	GLHelpers::windowResize(window, window_length, window_height);

	glfwSetWindowUserPointer(window, (void*)universe());

	// Color & depth
   glClearColor(0.f, 0.f, 0.f, 0.0f);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glfwSwapInterval(1);
   glfwSwapBuffers(window);
}

void Renderer::initShaders()
{
	std::printf("loading shaders\n");

	// Load shader program
	universe()->loadShader(
		"orbit",
		"shader/orbit.vert",
		"shader/orbit.frag");

	Shader* orbit_shader = universe()->shaders["orbit"];
	orbit_shader->activate();

	glUseProgram(0);

	// Load astrobody shader
	universe()->loadShader(
		"astrobody",
		"shader/astrobody.vert",
		"shader/astrobody.frag");

	Shader* astro_shader = universe()->shaders["astrobody"];
	astro_shader->activate();

	GLint light_pos_uni = glGetUniformLocation(astro_shader->getProgram(), "light_pos");
	glUniform3f(light_pos_uni, 0.f, 0.f, 0.f);

	GLint light_color_uni = glGetUniformLocation(astro_shader->getProgram(), "light_color");
	glUniform3f(light_color_uni, 1.f, 1.f, 1.f);

	glUseProgram(0);
}

void Renderer::initMeshes()
{
	std::printf("building meshes\n");
	universe()->active_camera = &universe()->system_camera;

	universe()->circle_mesh.setShader(universe()->shaders["orbit"]);
	universe()->circle_mesh.setRenderMode(GL_LINE_LOOP);
	universe()->circle_mesh.circle();

	universe()->sphere_mesh.setShader(universe()->shaders["astrobody"]);
	universe()->sphere_mesh.setRenderMode(GL_TRIANGLES);
	universe()->sphere_mesh.sphere();
}

void Renderer::initGUI()
{
	std::printf("initializing GUI\n");
	{
		using namespace CEGUI;

		gui_renderer = &OpenGL3Renderer::bootstrapSystem();
		gui_renderer->enableExtraStateSettings(true);

		DefaultResourceProvider* rp = static_cast<DefaultResourceProvider*>(
		    CEGUI::System::getSingleton().getResourceProvider());
		rp->setResourceGroupDirectory("schemes", "./data/gui/schemes/");
		rp->setResourceGroupDirectory("imagesets", "./data/gui/imagesets/");
		rp->setResourceGroupDirectory("fonts", "./data/gui/fonts/");
		rp->setResourceGroupDirectory("layouts", "./data/gui/layouts/");
		rp->setResourceGroupDirectory("looknfeels", "./data/gui/looknfeel/");
		rp->setResourceGroupDirectory("lua_scripts", "./data/gui/lua_scripts/");

		CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
		CEGUI::Font::setDefaultResourceGroup("fonts");
		CEGUI::Scheme::setDefaultResourceGroup("schemes");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
		CEGUI::WindowManager::setDefaultResourceGroup("layouts");
		CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

		SchemeManager::getSingleton().createFromFile( "TaharezLook.scheme" );
		FontManager::getSingleton().createFromFile( "DejaVuSans-10.font" );

		GUIContext& gui_context = System::getSingleton().getDefaultGUIContext();

		gui_context.setDefaultFont( "DejaVuSans-10" );
		gui_context.getMouseCursor().setDefaultImage(
			"TaharezLook/MouseArrow" );
		gui_context.setDefaultTooltipType( "TaharezLook/Tooltip" );

		WindowManager& window_manager = WindowManager::getSingleton();
		Window* root_window = window_manager.loadLayoutFromFile("test.xml");
		System::getSingleton().getDefaultGUIContext().setRootWindow(root_window);
	}
}

void Renderer::init()
{
	std::printf("RENDER INIT\n");

	initGLFW();
	initShaders();
	initMeshes();
	initGUI();

	GLHelpers::checkError("render init");
}

void Renderer::process()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	render3DScene();
	renderGUI();

	glfwSwapBuffers(universe()->window);

	updateProcessTime();
	GLHelpers::checkError("render");
}

void Renderer::close()
{
	std::printf("RENDER CLOSE\n");

	// Terminate GUI system
	if(gui_renderer != nullptr)
	{
		CEGUI::System::destroy();
		CEGUI::OpenGL3Renderer::destroy(static_cast<CEGUI::OpenGL3Renderer&>(*gui_renderer));
		gui_renderer = nullptr;
	}

	// Terminate GLFW
	if(universe()->window != nullptr)
	{
		glfwDestroyWindow(universe()->window);
		glfwTerminate();

		universe()->window = nullptr;
	}
}

void Renderer::render3DScene()
{
	 // We enable depth for 3D scene rendering only, so it doesn't affect GUI rendering
	glEnable(GL_DEPTH_TEST);

	double scale = universe()->SCALE;
	glm::mat4 view;
	if(universe()->active_camera != nullptr)
	{
		view = universe()->active_camera->getViewMatrix(scale);
	}else{
		view = glm::lookAt(
			glm::vec3(3.0, 3.0, 3.0),
			glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f));
	}

	glm::mat4 projection = glm::perspective(
		glm::radians(Renderer::FOV),
		window_length / window_height,
		Renderer::NEAR,
		Renderer::FAR*universe()->SCALE);

	for(std::map<std::string, Astrobody*>::iterator it = universe()->astrobodies.begin();
		it != universe()->astrobodies.end();
		++it)
	{
		Astrobody* astro = it->second;
		if(astro != nullptr)
		{
			astro->physical.draw(view, projection, scale);
		}
	}

	glDisable(GL_DEPTH_TEST);
}

void Renderer::renderGUI()
{
	// Updates CEGUI time, necessary for window fades
	// It's necessary to feed time pulse into both, due to an oddity in CEGUI
	double time_pulse = glfwGetTime()-getLastProcessTime();
	CEGUI::System::getSingleton().injectTimePulse(time_pulse);
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(time_pulse);

	if(gui_renderer != nullptr)
	{
		gui_renderer->beginRendering();
		CEGUI::System::getSingleton().renderAllGUIContexts();
		gui_renderer->endRendering();
	}
}
