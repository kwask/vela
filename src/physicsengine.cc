#include "physicsengine.h"
#include "physical.h"

PhysicsEngine::PhysicsEngine(Universe* universe)
	: Process(universe) {}

void PhysicsEngine::init()
{
	std::printf("PHYSICSENGINE INIT\n");
	std::printf("gravity: %E\n", Constants::GRAVITY);
	std::printf("density: %E\n", Constants::DENSITY);
}

void PhysicsEngine::process()
{
	double timescale = universe()->timescale;

	for(std::map<std::string, Astrobody*>::iterator it = universe()->astrobodies.begin();
		it != universe()->astrobodies.end();
		++it)
	{
		Astrobody* astro = it->second;
		if(astro != nullptr)
		{
			astro->physical.updateAnomaly(timescale);
		}
	}

	updateProcessTime();
}

void PhysicsEngine::close()
{
	std::printf("PHYSICSENGINE CLOSE\n");
}
