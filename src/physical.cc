#include <cmath>

#include "physical.h"

Physical::Physical() {}

Physical::Physical(const Physical &physical)
{
	this->object = physical.object;
	this->orbit = physical.orbit;
	this->primary = physical.primary;

	this->mass = physical.mass;
	this->angular_velocity = physical.angular_velocity;

	// Polar coordinates
	this->orbit_radius = physical.orbit_radius;
	this->true_anomaly = physical.true_anomaly;

	this->body_radius = physical.body_radius;
	this->luminosity = physical.luminosity;

	calcVolume();
	calcBodyRadius();
	calcAngularVelocity();
}

Physical::	Physical(double mass, double orbit_radius, double true_anomaly, double luminosity)
	: mass(mass), orbit_radius(orbit_radius), true_anomaly(true_anomaly), luminosity(luminosity) {}

Physical::~Physical()
{
	object = nullptr;
	orbit = nullptr;
}

Physical& Physical::operator=(const Physical &physical)
{
	this->object = physical.object;
	this->orbit = physical.orbit;
	this->primary = physical.primary;

	this->mass = physical.mass;
	this->angular_velocity = physical.angular_velocity;

	// Polar coordinates
	this->orbit_radius = physical.orbit_radius;
	this->true_anomaly = physical.true_anomaly;

	this->body_radius = physical.body_radius;
	this->luminosity = physical.luminosity;

	calcVolume();
	calcBodyRadius();
	calcAngularVelocity();

	return *this;
}

double Physical::getBodyRadius() const
{
	return body_radius;
}

double Physical::getMass() const
{
	return mass;
}

double Physical::getOrbitRadius() const
{
	return orbit_radius;
}

double Physical::getTrueAnomaly() const
{
	return true_anomaly;
}

double Physical::getAngularVelocity() const
{
	return angular_velocity;
}

double Physical::getVolume() const
{
	return volume;
}

void Physical::updateAnomaly(double dTime)
{
	true_anomaly = getTrueAnomaly()+(dTime*getAngularVelocity());
}

void Physical::draw(const glm::mat4 &view, const glm::mat4 &projection, double scale)
{
	if(object != nullptr){
		object->draw(view, projection, glm::vec3(getBodyX(), getBodyY(), 0.f), getBodyRadius(), scale, luminosity);
	}

	if(orbit != nullptr){
		if(primary != nullptr)
		{
			orbit->draw(view, projection, glm::vec3(primary->getBodyX(), primary->getBodyY(), 0.f), getOrbitRadius(), scale);
		}else{
			orbit->draw(view, projection);
		}
	}
}

double Physical::calcAngularVelocity()
{
	angular_velocity = 0;
	if(orbit_radius != 0)
	{
		angular_velocity = calcLinearVelocity()/orbit_radius;
	}

	return angular_velocity;
}

double Physical::calcLinearVelocity()
{
	double v = 0;

	if(primary != nullptr && orbit_radius != 0)
	{
		double u = Constants::GRAVITY*primary->mass;
		v = sqrt(u/orbit_radius);
	}

	return v;
}

double Physical::calcBodyRadius()
{
	double v = getVolume()*(3.f/(4.f*M_PI));
	body_radius = pow(v, 1.f/3.f);

	return body_radius;
}

double Physical::calcVolume()
{
	double m = getMass();
	double r = getBodyRadius();
	if(m > 0)
	{
		volume = m/Constants::DENSITY;
	}else if(r > 0)
	{
		volume = (4.f/3.f)*M_PI*pow(r, 3);
	}

	return volume;
}

double Physical::getBodyX()
{
	double x = orbit_radius*cos(true_anomaly);
	if(primary != nullptr)
	{
		x += primary->getBodyX();
	}

	return x;
}

double Physical::getBodyY()
{
	double y = orbit_radius*sin(true_anomaly);
	if(primary != nullptr)
	{
		y += primary->getBodyY();
	}

	return y;
}
