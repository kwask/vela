#include <iostream>
#include <map>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include "mesh.h"
#include "shader.h"
#include "edge.h"
#include "vertex.h"
#include "glhelpers.h"

Mesh::Mesh(Shader* shader, GLenum render_mode)
	:   shader(shader),
		render_mode(render_mode) {}

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, Shader* shader, GLenum render_mode)
	: vertices(vertices),
	  indices(indices),
	  shader(shader),
	  render_mode(render_mode)
{
	init();
}

GLuint Mesh::triangleCount() const
{
	return indices.size()/3;
}

void Mesh::setShader(Shader* shader)
{
	this->shader = shader;
}

void Mesh::setRenderMode(GLenum render_mode)
{
	this->render_mode = render_mode;
}

void Mesh::init()
{
	if(vertices.empty() || indices.empty())
	{
		return;
	}

	// Calculating our normal vectors
	calcNormals();

	// Generating VAO
	// VAOs define the relationship between VBOs and shaders
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Generating EBO
	// EBOs allow you to reduce duplicate point data by defining what idices can be reused and where
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
	 	indices.size()*sizeof(GLuint),
		&indices[0],
		GL_STATIC_DRAW
	);

	// Generating VBO
	// VBOs define vertex data, including position and color
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(
		GL_ARRAY_BUFFER,
		vertices.size()*sizeof(Vertex),
		&vertices[0],
		GL_STATIC_DRAW
	);

	// Linking our attributes
	GLint position_att = -1;
	GLint color_att = -1;
	GLint normal_att = -1;
	size_uni = -1;
	scale_uni = -1;
	model_uni = -1;
	view_uni = -1;
	project_uni = -1;
	luminosity_uni = -1;

	if(shader != nullptr)
	{
		shader->activate();
		position_att = glGetAttribLocation(shader->getProgram(), "local");
		color_att = glGetAttribLocation(shader->getProgram(), "color");
		normal_att = glGetAttribLocation(shader->getProgram(), "normal");

		size_uni = glGetUniformLocation(shader->getProgram(), "size");
		scale_uni = glGetUniformLocation(shader->getProgram(), "scale");
		model_uni = glGetUniformLocation(shader->getProgram(), "model");
		view_uni = glGetUniformLocation(shader->getProgram(), "view");
		project_uni = glGetUniformLocation(shader->getProgram(), "projection");
		luminosity_uni = glGetUniformLocation(shader->getProgram(), "light_luminosity");

		GLHelpers::checkError("get attributes");
		glUseProgram(0);
	}else{
		std::printf("shader was nullptr\n");
	}

	if(position_att >= 0)
	{
		glEnableVertexAttribArray(position_att);
		glVertexAttribPointer(
			position_att,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(Vertex),
			(void*)0
		);
	}else{
		std::printf("position attrib was not found\n");
	}

	if(color_att >= 0)
	{
		glEnableVertexAttribArray(color_att);
		glVertexAttribPointer(
			color_att,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(Vertex),
			(void*)offsetof(Vertex, color)
		);
	}else{
		std::printf("color attrib was not found\n");
	}

	if(normal_att >= 0)
	{
		glEnableVertexAttribArray(normal_att);
		glVertexAttribPointer(
			normal_att,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(Vertex),
			(void*)offsetof(Vertex, normal)
		);
	}else{
		std::printf("normal attrib was not found\n");
	}

	glBindVertexArray(0);
	glUseProgram(0);
	GLHelpers::checkError("mesh init");
}

void Mesh::draw(const glm::mat4 &view, const glm::mat4 &projection, const glm::vec3 &position, const double &size, const double &scale, const double &luminosity)
{
	shader->activate();
	glBindVertexArray(vao);
	GLHelpers::checkError("bind vao");

	// Passing uniform data
	if(model_uni >= 0)
	{
		glm::mat4 model;
		model = glm::translate(model, position);
		GLHelpers::checkError("trans");
		glUniformMatrix4fv(model_uni, 1, GL_FALSE, glm::value_ptr(model));
		GLHelpers::checkError("model");
	}

	if(size_uni >= 0)
	{
		glm::mat4 size_mat;
		size_mat = glm::scale(size_mat, glm::vec3(size, size, size));
		glUniformMatrix4fv(size_uni, 1, GL_FALSE, glm::value_ptr(size_mat));
		GLHelpers::checkError("size");
	}

	if(scale_uni >= 0)
	{
		glm::mat4 scale_mat;
		scale_mat = glm::scale(scale_mat, glm::vec3(scale, scale, scale));
		glUniformMatrix4fv(scale_uni, 1, GL_FALSE, glm::value_ptr(scale_mat));
		GLHelpers::checkError("scale");
	}

	if(view_uni >= 0)
	{
		glUniformMatrix4fv(view_uni, 1, GL_FALSE, glm::value_ptr(view));
	}

	if(project_uni >= 0)
	{
		glUniformMatrix4fv(project_uni, 1, GL_FALSE, glm::value_ptr(projection));
	}

	if(luminosity_uni >= 0)
	{
		glUniform3f(luminosity_uni, luminosity, luminosity, luminosity);
	}

	glDrawElements(render_mode, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glUseProgram(0);
	
	GLHelpers::checkError("draw");
}

void Mesh::circle()
{
	uint32_t vertice_count = 72000;
	double degrees_per_vert = 360.f/vertice_count;

	for(uint32_t i = 0; i < vertice_count; i++)
	{
		Vertex v(
			glm::vec3(
				cos(glm::radians(i*degrees_per_vert)),
				sin(glm::radians(i*degrees_per_vert)),
				0),
			glm::vec3(1.f, 1.f, 1.f));
		vertices.emplace_back(v);
		indices.emplace_back(i);
	}

	init();
}

void Mesh::ellipses(double semimajor, double semiminor)
{
	uint32_t vertice_count = (uint32_t)(6.28f*semimajor*180.f);

	double degrees_per_vert = 360.f/vertice_count;

	for(uint32_t i = 0; i < vertice_count; i++)
	{
		Vertex v(
			glm::vec3(
				cos(glm::radians(i*degrees_per_vert)),
				sin(glm::radians(i*degrees_per_vert)),
				0),
			glm::vec3(1.f, 1.f, 1.f));
		vertices.emplace_back(v);
		indices.emplace_back(i);
	}

	init();
}

void Mesh::sphere()
{
	const double t = (1.0 + sqrt(5.0)) / 2.0;

	vertices.emplace_back(Vertex(
		glm::vec3(-1.0,  t, 0.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3( 1.0,  t, 0.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(-1.0, -t, 0.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3( 1.0, -t, 0.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(0.0, -1.0,  t),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(0.0,  1.0,  t),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(0.0, -1.0, -t),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(0.0,  1.0, -t),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3( t, 0.0, -1.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3( t, 0.0,  1.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(-t, 0.0, -1.0),
		glm::vec3(1.f, 1.f, 1.f)));
	vertices.emplace_back(Vertex(
		glm::vec3(-t, 0.0,  1.0),
		glm::vec3(1.f, 1.f, 1.f)));

	addTriangle(0, 11, 5);
	addTriangle(0, 5, 1);
	addTriangle(0, 1, 7);
	addTriangle(0, 7, 10);
	addTriangle(0, 10, 11);
	addTriangle(1, 5, 9);
	addTriangle(5, 11, 4);
	addTriangle(11, 10, 2);
	addTriangle(10, 7, 6);
	addTriangle(7, 1, 8);
	addTriangle(3, 9, 4);
	addTriangle(3, 4, 2);
	addTriangle(3, 2, 6);
	addTriangle(3, 6, 8);
	addTriangle(3, 8, 9);
	addTriangle(4, 9, 5);
	addTriangle(2, 4, 11);
	addTriangle(6, 2, 10);
	addTriangle(8, 6, 7);
	addTriangle(9, 8, 1);

	subdivideMesh();
	subdivideMesh();
	subdivideMesh();

	init();
}

void Mesh::addTriangle(GLuint a, GLuint b, GLuint c)
{
	indices.emplace_back(a);
	indices.emplace_back(b);
	indices.emplace_back(c);
}

void Mesh::addQuad(GLuint a, GLuint b, GLuint c, GLuint d)
{
	indices.emplace_back(a);
	indices.emplace_back(b);
	indices.emplace_back(c);

	indices.emplace_back(a);
	indices.emplace_back(c);
	indices.emplace_back(d);
}

uint32_t Mesh::subdivideEdge(uint32_t f0, uint32_t f1, const Vertex &v0, const Vertex &v1, Mesh &mesh, std::map<Edge, uint32_t> &divisions)
{
	const Edge edge(f0, f1);
	auto it = divisions.find(edge);
	if (it != divisions.end())
	{
		return it->second;
	}

	const glm::vec3 v = glm::normalize(glm::vec3(0.5) * (v0.pos + v1.pos));
	const uint32_t f = mesh.vertices.size();
	mesh.vertices.emplace_back(Vertex(v));
	divisions.emplace(edge, f);
	return f;
}

void Mesh::subdivideMesh()
{
	Mesh mesh_out;
	mesh_out.vertices = this->vertices;

	std::map<Edge, uint32_t> divisions; // Edge -> new vertex

	for (uint32_t i = 0; i < this->triangleCount(); ++i)
	{
		const uint32_t f0 = this->indices[i * 3];
		const uint32_t f1 = this->indices[i * 3 + 1];
		const uint32_t f2 = this->indices[i * 3 + 2];

		const Vertex v0 = this->vertices[f0];
		const Vertex v1 = this->vertices[f1];
		const Vertex v2 = this->vertices[f2];

		const uint32_t f3 = subdivideEdge(f0, f1, v0, v1, mesh_out, divisions);
		const uint32_t f4 = subdivideEdge(f1, f2, v1, v2, mesh_out, divisions);
		const uint32_t f5 = subdivideEdge(f2, f0, v2, v0, mesh_out, divisions);

		mesh_out.addTriangle(f0, f3, f5);
		mesh_out.addTriangle(f3, f1, f4);
		mesh_out.addTriangle(f4, f2, f5);
		mesh_out.addTriangle(f3, f4, f5);
	}

	this->vertices.clear();
	this->vertices = mesh_out.vertices;

	this->indices.clear();
	this->indices = mesh_out.indices;
}

void Mesh::calcNormals()
{
	for (uint32_t i = 0; i < this->triangleCount(); ++i)
	{
		const uint32_t f0 = this->indices[i * 3];
		const uint32_t f1 = this->indices[i * 3 + 1];
		const uint32_t f2 = this->indices[i * 3 + 2];

		Vertex* v0 = &this->vertices[f0];
		Vertex* v1 = &this->vertices[f1];
		Vertex* v2 = &this->vertices[f2];

		glm::vec3 normal = calcFaceNormal(*v0, *v1, *v2);

		v0->normal += normal;
		v1->normal += normal;
		v2->normal += normal;
	}

	for (uint32_t i = 0; i < vertices.size(); ++i)
	{
		Vertex* v = &this->vertices[i];
		v->normal = glm::normalize(v->normal);

	}
}

glm::vec3 Mesh::calcFaceNormal(const Vertex &v0, const Vertex &v1, const Vertex &v2)
{
	glm::vec3 normal(0.f, 0.f, 0.f);

	normal.x += (v0.pos.y-v1.pos.y)*(v0.pos.z+v1.pos.z);
	normal.y += (v0.pos.z-v1.pos.z)*(v0.pos.x+v1.pos.x);
	normal.z += (v0.pos.x-v1.pos.x)*(v0.pos.y+v1.pos.y);

	normal.x += (v1.pos.y-v2.pos.y)*(v1.pos.z+v2.pos.z);
	normal.y += (v1.pos.z-v2.pos.z)*(v1.pos.x+v2.pos.x);
	normal.z += (v1.pos.x-v2.pos.x)*(v1.pos.y+v2.pos.y);

	normal.x += (v2.pos.y-v0.pos.y)*(v2.pos.z+v0.pos.z);
	normal.y += (v2.pos.z-v0.pos.z)*(v2.pos.x+v0.pos.x);
	normal.z += (v2.pos.x-v0.pos.x)*(v2.pos.y+v0.pos.y);

	return glm::normalize(normal);
}
