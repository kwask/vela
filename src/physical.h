#ifndef PHYSICAL_H
#define PHYSICAL_H

#include "mesh.h"

namespace Constants
{
	const double GRAVITY = 6.674E-17f; // km^3 * Mg^-1 * s^-2
	const double DENSITY = 5.515E+9f; // Mg/km^3
}

// This class is used by all objects that move in space.
class Physical
{
public:
	Physical();
	Physical(const Physical &physical);
	Physical(double mass, double radius, double true_anomaly, double luminosity = 0.f);
	~Physical();

	Mesh* object = nullptr;
	Mesh* orbit = nullptr;

	Physical* primary = nullptr; // The body that this one revolves around, if this is null, then it must be the center of the solar system

	Physical& operator=(const Physical &physical);

	double getBodyRadius() const;
	double getMass() const;
	double getOrbitRadius() const;
	double getTrueAnomaly() const;
	double getAngularVelocity() const;
	double getVolume() const;

	void updateAnomaly(double dTime);
	void draw(const glm::mat4 &view, const glm::mat4 &projection, double scale = 1000.f);
private:
	double calcAngularVelocity();
	double calcLinearVelocity();
	double calcBodyRadius();
	double calcVolume();
	double getBodyX();
	double getBodyY();

	double mass = 0;
	double angular_velocity = 0; // measured in radians/second

	// Polar coordinates
	double orbit_radius = 0;
	double true_anomaly = 0; // Where body is at current time, this changes every iteration, measured in radians

	double volume = 0;
	double body_radius = 0;
	double luminosity = 0;
};

#endif
