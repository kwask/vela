#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "astrobody.h"
#include "vertex.h"

Astrobody::Astrobody(uint64_t identifier)
	: Basic(identifier) {}

Astrobody::Astrobody(uint64_t identifier, const Physical &phys)
		: Basic(identifier), physical(phys) {}

Astrobody::~Astrobody() {}

void Astrobody::parse(instruct_set* instructions)
{

}

void Astrobody::genOrbitMesh()
{

}
