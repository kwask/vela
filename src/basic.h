#ifndef BASIC_H
#define BASIC_H

#include <queue>
#include <string>

typedef std::queue<std::string> instruct_set;

// This is the building block for all items that can be defined by .dat files
class Basic
{
public:
	Basic(uint64_t identifier);
	virtual ~Basic();

	uint64_t ident();

	void setName(const std::string name);
	virtual void parse(instruct_set* instructions) = 0; // All spawnable objects should be able to be parsed from an instruction set

	std::string getName() const;
private:
	uint64_t identifier = 0;

	std::string name;
};

#endif
