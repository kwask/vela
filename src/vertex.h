#ifndef VERTEX_H
#define VERTEX_H

#include <glm/glm.hpp>

typedef glm::vec3 Position;
typedef glm::vec3 Color;
typedef glm::vec3 Normal;

struct Vertex {
	Vertex(Position pos, Color color = Color(1.f, 1.f, 1.f));

	Position pos;
	Color color;
	Normal normal;
};

#endif
