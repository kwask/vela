#ifndef GENERATOR_H
#define GENERATOR_H

#include <fstream>
#include <string>
#include <queue>
#include <boost/filesystem.hpp>

#include "process.h"
#include "basic.h"
#include "universe.h"

namespace filesystem = boost::filesystem;

class Generator : protected Process
{
public:
	Generator(Universe* universe);

	void init() override;
	void process() override;
	void close() override;

	static const uint32_t MAX_LINE_SIZE = 256;
	static const uint32_t MAX_FILE_LINES = 65536;
	static const uint32_t MAX_INSTRUCTIONS = 65536;

private:
	void initSolarSystems();
	void initNameGens();

	instruct_set* read(filesystem::path dir);

	void parse(instruct_set* instructions);
	void parseDir(filesystem::path dir);

	std::string material_dir = "data/materials";
	std::string solarsystem_dir = "data/solarsystems";
	std::string life_dir = "data/life";
	std::string name_dir = "data/names";

	// Name generation
	NameGenerator name_gen;
};

#endif
