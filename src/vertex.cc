#include <glm/glm.hpp>
#include <cstdlib>

#include "vertex.h"

typedef glm::vec3 Position;
typedef glm::vec3 Color;
typedef glm::vec3 Normal;

Vertex::Vertex(Position pos, Color color)
	: 	pos(normalize(pos)),
		color(color),
		normal(Normal(0.f, 0.f, 0.f)) {}
