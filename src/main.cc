#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <thread>
#include <chrono>

#include "universe.h"
#include "generator.h"
#include "renderer.h"
#include "glhelpers.h"
#include "input.h"
#include "physicsengine.h"

int main ()
{
	const double MILLISECONDS_PER_SECOND = 1000.f;

	Universe universe;

	Renderer renderer(&universe);
	Input input(&universe);
	Generator generator(&universe);
	PhysicsEngine physicsengine(&universe);

	renderer.init(); // Needs to be first to set up context for input
	input.init();
	generator.init();
	physicsengine.init();

	double last_time;

	std::printf("MAIN LOOP START\n");
	while(!universe.exit)
	{
		last_time = glfwGetTime();

		input.process();
		if(!universe.paused)
		{
			generator.process();
			physicsengine.process();
		}
		renderer.process();

		double seconds_per_frame = 1.f/renderer.frames_per_second;
		double d_time = glfwGetTime()-last_time;
		if(d_time < seconds_per_frame)
		{
			long wait_time = (long)(MILLISECONDS_PER_SECOND*(seconds_per_frame-d_time));

			std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
		}
	}
	std::printf("MAIN LOOP END\n");

	physicsengine.close();
	generator.close();
	input.close();
	renderer.close();
}
