#ifndef COUNTER_H
#define COUNTER_H

#include <stdint.h>

class Counter
{
public:
	uint64_t increment();
private:
	uint64_t count = 0;
};

#endif
