Vela is a very simple orbital sim, basically my first foray into 3D graphics.

This project uses these following dependencies:
 * C++ Boost libraries
 * GLM OpenGL Mathematics
 * GLFW 3.2
 * CEGUI 0.8.7
